import {useContext} from 'react';
import {DataContext} from '../contexts';

export const useData = () => {
  const dataContext = useContext(DataContext);

  return dataContext;
};
