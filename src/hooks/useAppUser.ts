import {useContext} from 'react';
import {UserContext} from '../contexts';
import {UserContextType} from '../types';

export const useAppUser = (): UserContextType => {
  const userContext = useContext<UserContextType>(UserContext);

  return userContext;
};
