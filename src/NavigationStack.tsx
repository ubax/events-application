import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {StackParamList} from './types';
import {
  COLORS,
  DURATION_SCREEN,
  DETAILS_SCREEN,
  MEETINGS_SCREEN,
  PARTICIPANTS_SCREEN,
  SUMMARY_SCREEN,
  HOME_SCREEN,
} from './constants';
import {
  ParticipantsScreen,
  DurationScreen,
  MeetingsScreen,
  DetailsScreen,
  SummaryScreen,
  HomeScreen,
} from './screens';

const Stack = createStackNavigator<StackParamList>();

export function NavigationStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerTintColor: COLORS.primary,
        }}>
        <Stack.Screen name={HOME_SCREEN} component={HomeScreen} />
        <Stack.Screen
          name={DURATION_SCREEN}
          component={DurationScreen}
          options={{title: 'Initial parameters'}}
        />
        <Stack.Screen
          name={PARTICIPANTS_SCREEN}
          component={ParticipantsScreen}
        />
        <Stack.Screen name={MEETINGS_SCREEN} component={MeetingsScreen} />
        <Stack.Screen name={DETAILS_SCREEN} component={DetailsScreen} />
        <Stack.Screen name={SUMMARY_SCREEN} component={SummaryScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
