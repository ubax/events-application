import dayjs from 'dayjs';
import React, {useCallback, useMemo} from 'react';
import {Text} from 'react-native';
import {
  Button,
  Header,
  MeetingsTimeList,
  ScreenWrapper,
  Spacer,
  ParticipantsChipList,
} from '../components';
import {DETAILS_SCREEN} from '../constants';
import {useData} from '../hooks';
import {
  Meeting,
  MeetingsScreenProps,
  MeetingTime,
  User,
  HourMinutesTime,
} from '../types';
import {timeToString} from '../utils';
import {findTimesForMeeting} from '../utils';

const isDefined = <T extends unknown>(x: T | undefined): x is T =>
  x !== undefined;

const timeInputToString = (x: HourMinutesTime) =>
  `${timeToString(x.hour)}:${timeToString(x.minutes)}`;

const joinParticipantsMeetings = (
  participants: ReadonlyArray<User>,
  getMeetingsForParticipant: (user: User) => Meeting[],
) => {
  return participants.reduce<Meeting[]>(
    (result, user) => [...result, ...getMeetingsForParticipant(user)],
    [],
  );
};

export const MeetingsScreen = (props: MeetingsScreenProps) => {
  const {participants, duration, startTime, finishTime} = props.route.params;
  const {data} = useData();
  const getMeetingsForParticipant = useCallback(
    ({meetingIds}: User): Meeting[] =>
      meetingIds
        .map(mId => data.meetings.find(m => m.id === mId))
        .filter(isDefined),
    [data],
  );
  const meetings = useMemo(
    () =>
      findTimesForMeeting({
        participantsMeetings: joinParticipantsMeetings(
          participants,
          getMeetingsForParticipant,
        ),
        earliestStartTime: timeInputToString(startTime),
        latestEndTime: timeInputToString(finishTime),
        minDurationInMinutes: duration,
        datesRange: {
          from: dayjs().toDate(),
          to: dayjs()
            .set('hour', 0)
            .set('minute', 0)
            .set('second', 0)
            .add(8, 'days')
            .toDate(),
        },
      }),
    [participants, duration, startTime, finishTime, getMeetingsForParticipant],
  );

  console.log(meetings);

  const [selectedMeeting, setSelectedMeeting] = React.useState<
    MeetingTime | undefined
  >(undefined);
  const handleGoForward = () => {
    if (selectedMeeting) {
      props.navigation.push(DETAILS_SCREEN, {
        duration,
        participants,
        meetingTime: selectedMeeting,
      });
    }
  };

  return (
    <ScreenWrapper>
      <Text>
        Choose the most suitable time range for the meeting. You will select
        exact date in the next step
      </Text>
      <Spacer size={3} />
      <Header variant="h3">Duration</Header>
      <Spacer size={1} />
      <Text>{duration} minutes</Text>
      <Spacer size={3} />
      <Header variant="h3">Participants</Header>
      <Spacer size={1} />
      <ParticipantsChipList participants={participants} />
      <Spacer size={3} />

      <Header variant="h3">Date ranges</Header>
      <Spacer size={1} />
      <MeetingsTimeList
        meetings={meetings}
        onSelectMeeting={meeting => setSelectedMeeting(meeting)}
        selectedMeeting={selectedMeeting}
        showDifference
      />
      <Spacer size={3} />

      <Button
        onPress={handleGoForward}
        disabled={selectedMeeting === undefined}
        title="Select date range"
        style="primary"
      />
    </ScreenWrapper>
  );
};
