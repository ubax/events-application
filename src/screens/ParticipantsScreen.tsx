import React, {useState} from 'react';
import {Text} from 'react-native';
import {ParticipantsScreenProps, User} from '../types';
import {
  UserSelectList,
  Button,
  ScreenWrapper,
  Spacer,
  ParticipantsList,
} from '../components';
import {MEETINGS_SCREEN} from '../constants';
import {useAppUser, useData} from '../hooks';
import {UserScheduleBottomSheet} from '../components';

function addAppUser(
  participants: ReadonlyArray<User>,
  appUser: User | undefined,
): User[] {
  return [...participants, ...(appUser ? [appUser] : [])];
}

export const ParticipantsScreen = ({
  navigation,
  route,
}: ParticipantsScreenProps) => {
  const {data} = useData();
  const {user: appUser} = useAppUser();
  const [userForSchedule, setUserForSchedule] = useState<User | undefined>(
    undefined,
  );
  const [participants, setParticipants] = useState<ReadonlyArray<User>>([]);
  const availableUsers = data.users.filter(
    ({id}) =>
      !participants.find(({id: participantId}) => participantId === id) &&
      id !== appUser?.id,
  );
  return (
    <ScreenWrapper>
      <Text>
        Now click users that should participate. To see user schedule long tap
        on them.
      </Text>
      <Spacer size={3} />
      <UserSelectList
        users={availableUsers}
        onShowUserSchedule={user => setUserForSchedule(user)}
        onSelectUser={user => setParticipants(prev => [user, ...prev])}
      />
      <Spacer size={3} />
      <ParticipantsList
        participants={participants}
        onShowUserSchedule={user => setUserForSchedule(user)}
        onRemoveParticipant={participant =>
          setParticipants(prev => prev.filter(({id}) => id !== participant.id))
        }
      />
      <Button
        style="primary"
        disabled={participants.length === 0}
        onPress={() =>
          navigation.navigate<typeof MEETINGS_SCREEN>(MEETINGS_SCREEN, {
            ...route.params,
            participants: addAppUser(participants, appUser),
          })
        }
        title="Show suggestions"
      />
      <UserScheduleBottomSheet
        isOpen={!!userForSchedule}
        user={userForSchedule}
        meetings={data.meetings}
        onClose={() => setUserForSchedule(undefined)}
      />
    </ScreenWrapper>
  );
};
