import dayjs from 'dayjs';
import React from 'react';
import {Text} from 'react-native';
import {
  Button,
  DateTimeInput,
  Header,
  RoomsList,
  ScreenWrapper,
  Spacer,
  TextInput,
} from '../components';
import {ParticipantsChipList} from '../components';
import {SUMMARY_SCREEN} from '../constants';
import {useData} from '../hooks';
import {Room, DetailsScreenProps} from '../types';

export const DetailsScreen = (props: DetailsScreenProps) => {
  const {participants, duration, meetingTime} = props.route.params;
  const [selectedRoom, setSelectedRoom] = React.useState<Room | undefined>(
    undefined,
  );
  const [date, setDate] = React.useState<Date>(meetingTime.startTime);
  const [reason, setReason] = React.useState('');
  const {
    data: {rooms, meetings},
  } = useData();
  const simultaneousMeetings = meetings.filter(meeting => {
    const currentMeetingStart = dayjs(date);
    const currentMeetingEnd = currentMeetingStart.add(duration, 'minutes');
    const meetingStart = dayjs(meeting.startTime);
    const meetingEnd = dayjs(meeting.endTime);
    return (
      !currentMeetingStart.isBefore(meetingEnd) &&
      !currentMeetingEnd.isAfter(meetingStart)
    );
  });
  const freeRooms = rooms.filter(
    room => !simultaneousMeetings.find(meeting => meeting.roomId === room.id),
  );
  const sortedRooms = freeRooms.sort((r1, r2) => {
    const r1Delta = r1.capacity - participants.length;
    const r2Delta = r2.capacity - participants.length;
    if (r1Delta >= 0 && r2Delta >= 0) {
      return r1Delta - r2Delta;
    }
    if (r1Delta > r2Delta) {
      return -1;
    }
    return 1;
  });
  return (
    <ScreenWrapper>
      <Text>Choose a room and write the reason for the meeting.</Text>
      <Spacer size={3} />
      <Header variant="h3">Duration</Header>
      <Spacer size={1} />
      <Text>{duration} minutes</Text>
      <Spacer size={3} />
      <Header variant="h3">Participants</Header>
      <Spacer size={1} />
      <ParticipantsChipList participants={participants} />
      <Spacer size={3} />

      <Header variant="h3">Date</Header>
      <Spacer size={1} />
      <DateTimeInput
        value={date}
        minimumDate={meetingTime.startTime}
        maximumDate={dayjs(meetingTime.endTime)
          .subtract(duration, 'minutes')
          .toDate()}
        onChange={setDate}
      />
      <Spacer size={3} />

      <Header variant="h3">Room</Header>
      <Spacer size={1} />
      <RoomsList
        key={date.toISOString()}
        rooms={sortedRooms}
        onSelectRoom={room => setSelectedRoom(room)}
        selectedRoom={selectedRoom}
      />
      <Spacer size={3} />

      <Header variant="h3">Reason</Header>
      <Spacer size={1} />
      <TextInput
        rows={4}
        placeholder="Reason"
        value={reason}
        onChangeText={setReason}
      />
      <Spacer size={3} />

      <Button
        onPress={() =>
          props.navigation.navigate(SUMMARY_SCREEN, {
            meeting: {
              id: '1',
              userIds: participants.map(p => p.id),
              startTime: date,
              endTime: dayjs(date).add(duration, 'minutes').toDate(),
              roomId: selectedRoom!.id,
              purpose: reason,
            },
          })
        }
        title="Create meeting"
        style="primary"
        disabled={!selectedRoom || !reason || !date}
      />
    </ScreenWrapper>
  );
};
