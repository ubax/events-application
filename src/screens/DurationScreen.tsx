import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {DurationScreenProps, HourMinutesTime} from '../types';
import {
  Button,
  ScreenWrapper,
  Spacer,
  Header,
  DurationSelector,
  TimeInput,
} from '../components';
import {PARTICIPANTS_SCREEN} from '../constants';

export const DurationScreen = ({navigation}: DurationScreenProps) => {
  const [duration, setDuration] = useState<number | undefined>(undefined);
  const [startTime, setStartTime] = useState<HourMinutesTime>({
    hour: 9,
    minutes: 0,
  });
  const [finishTime, setFinishTime] = useState<HourMinutesTime>({
    hour: 16,
    minutes: 0,
  });

  const onSelectDuration = () => {
    if (duration) {
      navigation.navigate<typeof PARTICIPANTS_SCREEN>(PARTICIPANTS_SCREEN, {
        duration: duration,
        startTime,
        finishTime,
      });
    }
  };

  return (
    <ScreenWrapper>
      <View style={styles.contentContainer}>
        <Text>
          To organize the meeting first choose some initial parameters.
        </Text>
        <Spacer size={3} />
        <Header variant="h3">Duration</Header>
        <Spacer size={1} />
        <DurationSelector
          value={duration}
          onChange={setDuration}
          predefinedValues={[15, 30, 60]}
        />
        <Spacer size={3} />
        <Header variant="h3">Earliest starting hour</Header>
        <Spacer size={1} />
        <TimeInput
          placeholder="Start time"
          value={startTime}
          onChange={setStartTime}
        />
        <Spacer size={3} />
        <Header variant="h3">Latest finish hour</Header>
        <Spacer size={1} />
        <TimeInput
          placeholder="Finish time"
          value={finishTime}
          onChange={setFinishTime}
        />
      </View>
      <Button
        style="primary"
        disabled={!duration}
        onPress={onSelectDuration}
        title="Next"
      />
    </ScreenWrapper>
  );
};

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
  },
});
