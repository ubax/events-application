import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Spacer} from '../components';
import {COLORS} from '../constants';

export const LoadingScreen = () => {
  return (
    <View style={styles.contentContainer}>
      <Text style={styles.textStyle}>Meetings application</Text>
      <Spacer size={5} />
      <Text style={styles.smallTextStyle}>Designed & created by</Text>
      <Text style={styles.authorTextStyle}>Jakub Tkacz</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    backgroundColor: COLORS.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textStyle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: COLORS.white,
  },
  smallTextStyle: {
    fontSize: 18,
    fontWeight: '200',
    color: COLORS.white,
  },
  authorTextStyle: {
    fontSize: 18,
    fontWeight: '400',
    color: COLORS.white,
  },
});
