import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ScreenWrapper, Spacer, UserSelectList} from '../components';

import {DURATION_SCREEN} from '../constants';
import {useAppUser, useData} from '../hooks';

import {HomeScreenProps} from '../types';

export const HomeScreen = ({navigation}: HomeScreenProps) => {
  const {
    data: {users},
  } = useData();
  const {setUser} = useAppUser();

  return (
    <ScreenWrapper>
      <View style={styles.contentContainer}>
        <Text>Let us know who you are</Text>
        <Spacer size={1} />
        <UserSelectList
          users={users}
          onSelectUser={user => {
            setUser(user);
            navigation.replace(DURATION_SCREEN, {});
          }}
        />
      </View>
    </ScreenWrapper>
  );
};

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
  },
});
