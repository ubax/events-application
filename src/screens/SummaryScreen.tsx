import dayjs from 'dayjs';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {
  Button,
  Header,
  ScreenWrapper,
  Spacer,
  ParticipantsChipList,
} from '../components';
import {COLORS, INITIAL_FLOW_SCREEN} from '../constants';
import {useData} from '../hooks';

import {Meeting, SummaryScreenProps} from '../types';
import {addMeetingToState, addToCalendar} from '../utils';

export const SummaryScreen = (props: SummaryScreenProps) => {
  const {data, setData} = useData();
  const {meeting} = props.route.params;

  const createMeeting = (_meeting: Meeting) => {
    setData(prev => addMeetingToState(prev, _meeting));
  };

  const startDate = dayjs(meeting.startTime).format('YYYY-MM-DD');
  const startTime = dayjs(meeting.startTime).format('HH:mm');
  const endDate = dayjs(meeting.endTime).format('YYYY-MM-DD');
  const endTime = dayjs(meeting.endTime).format('HH:mm');
  const users = data.users.filter(user => meeting.userIds.includes(user.id));
  const room = data.rooms.find(r => r.id === meeting.roomId);
  return (
    <ScreenWrapper>
      <View style={styles.contentContainer}>
        <Header variant="h3">Participants</Header>
        <Spacer size={1} />
        <ParticipantsChipList participants={users} />
        <Spacer size={3} />

        <Header variant="h3">From</Header>
        <Spacer size={1} />
        <Text>
          {startDate} <Text style={styles.time}>{startTime}</Text>
        </Text>
        <Spacer size={3} />

        <Header variant="h3">To</Header>
        <Spacer size={1} />
        <Text>
          {endDate} <Text style={styles.time}>{endTime}</Text>
        </Text>
        <Spacer size={3} />

        <Header variant="h3">Room</Header>
        <Spacer size={1} />
        <Text>{room?.name ?? 'Unknown room'}</Text>
        <Spacer size={3} />

        <Header variant="h3">Reason</Header>
        <Text>{meeting.purpose}</Text>
        <Spacer size={3} />
      </View>

      <Button
        onPress={async () => {
          createMeeting(meeting);
          await addToCalendar(meeting);
          props.navigation.navigate(INITIAL_FLOW_SCREEN, {});
        }}
        title="Create and add to calendar"
        style="primary"
      />
      <Spacer size={3} />
      <Button
        onPress={() => {
          createMeeting(meeting);
          props.navigation.navigate(INITIAL_FLOW_SCREEN, {});
        }}
        title="Create without adding to calendar"
        style="primary"
      />
    </ScreenWrapper>
  );
};

const styles = StyleSheet.create({
  time: {
    fontWeight: 'bold',
    color: COLORS.primary,
  },
  contentContainer: {
    flex: 1,
  },
});
