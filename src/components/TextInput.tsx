import React from 'react';
import {
  Pressable,
  StyleSheet,
  Text,
  TextInput as RNTextInput,
  View,
} from 'react-native';
import {COLORS} from '../constants';

interface Props {
  value: string;
  placeholder: string;
  onChangeText: (text: string) => void;
  rows?: number;
  clearable?: boolean;
}

export function TextInput({
  value,
  placeholder,
  onChangeText,
  clearable,
  rows: _rows,
}: Props) {
  const rows = _rows ? parseInt(`${_rows}`) : 1;
  const heightStyle = {
    maxHeight: rows * 14 + 18,
    minHeight: rows * 14 + 18,
  };

  const clearValue = () => onChangeText('');

  return (
    <View style={[styles.container, heightStyle]}>
      <RNTextInput
        placeholderTextColor={COLORS.primary}
        style={[styles.main, heightStyle]}
        placeholder={placeholder}
        onChangeText={onChangeText}
        value={value}
        multiline={rows > 1}
      />
      {clearable && !!value && (
        <Pressable onPress={clearValue}>
          <Text style={styles.xButton}>X</Text>
        </Pressable>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    paddingHorizontal: 8,
    paddingVertical: 8,
    color: COLORS.primary,
  },
  container: {
    flex: 1,
    borderColor: COLORS.primary,
    borderWidth: 1,
    borderRadius: 6,
    flexDirection: 'row',
    alignItems: 'center',
  },
  xButton: {
    marginRight: 8,
    color: COLORS.primary,
    fontWeight: 'bold',
  },
});
