import React from 'react';
import {StyleSheet, View} from 'react-native';

interface Props {
  size: number;
}
export function Spacer({size}: Props) {
  return (
    <View
      style={[styles.container, {maxHeight: size * 4, minHeight: size * 4}]}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
