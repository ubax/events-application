import React, {useState} from 'react';
import DateTimePicker, {
  DateTimePickerEvent,
} from '@react-native-community/datetimepicker';
import {Platform, StyleSheet, View} from 'react-native';
import dayjs from 'dayjs';
import {SelectorItem} from '..';
import {TimeInputProps, TimeInput} from './TimeInput';

interface Props {
  value: Date | undefined;
  onChange: (time: Date) => void;
  minimumDate?: Date;
  maximumDate?: Date;
}

function AndroidDateTimeInput(props: Props) {
  const [isDateOpen, setIsDateOpen] = useState(false);

  const handleDateChange: Props['onChange'] = time => {
    setIsDateOpen(false);
    props.onChange(time);
  };

  const handleTimeChange: TimeInputProps['onChange'] = time => {
    const newDate = dayjs(props.value)
      .set('minutes', time.minutes)
      .set('hours', time.hour)
      .toDate();
    props.onChange(newDate);
  };

  const dateString = dayjs(props.value).format('YYYY-MM-DD');
  const timeValue = props.value
    ? {
        hour: props.value.getHours(),
        minutes: props.value.getMinutes(),
      }
    : undefined;

  return (
    <View style={styles.androidContainer}>
      <SelectorItem
        value={dateString}
        onPress={() => setIsDateOpen(true)}
        isSelected={false}
      />
      <TimeInput
        value={timeValue}
        onChange={handleTimeChange}
        placeholder="00:00"
      />
      {isDateOpen && (
        <DefaultDateTimeInput {...props} onChange={handleDateChange} />
      )}
    </View>
  );
}

function DefaultDateTimeInput({
  value,
  onChange,
  minimumDate,
  maximumDate,
}: Props) {
  const handleChange = (_: DateTimePickerEvent, date: Date | undefined) => {
    if (date) {
      onChange(date);
    }
  };
  const dateValue = value ?? new Date();
  return (
    <DateTimePicker
      value={dateValue}
      mode="datetime"
      onChange={handleChange}
      minimumDate={minimumDate}
      maximumDate={maximumDate}
    />
  );
}

export const DateTimeInput =
  Platform.OS === 'android' ? AndroidDateTimeInput : DefaultDateTimeInput;

const styles = StyleSheet.create({
  androidContainer: {
    flexDirection: 'row',
  },
});
