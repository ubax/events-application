import React, {useState} from 'react';
import DateTimePicker, {
  DateTimePickerEvent,
} from '@react-native-community/datetimepicker';
import dayjs from 'dayjs';
import {Platform, View} from 'react-native';
import {SelectorItem} from '../SelectorItem';
import {timeToString} from '../../utils';
import {HourMinutesTime} from '../../types';

interface Props {
  value: HourMinutesTime | undefined;
  placeholder: string;
  onChange: (time: HourMinutesTime) => void;
}

export type TimeInputProps = Props;

function AndroidTimeInput(props: Props) {
  const [isOpen, setIsOpen] = useState(false);

  const handleChange: Props['onChange'] = time => {
    setIsOpen(false);
    props.onChange(time);
  };

  const minutes = props.value?.minutes ?? 0;
  const hours = props.value?.hour ?? 0;

  const timeString = `${timeToString(hours)}:${timeToString(minutes)}`;
  console.log('TimeInput');
  return (
    <View>
      <SelectorItem
        value={timeString}
        onPress={() => setIsOpen(true)}
        isSelected={false}
      />
      {isOpen && <DefaultTimeInput {...props} onChange={handleChange} />}
    </View>
  );
}

function DefaultTimeInput({value, placeholder, onChange}: Props) {
  const handleChange = (_: DateTimePickerEvent, date: Date | undefined) => {
    if (date) {
      onChange({
        hour: date.getHours(),
        minutes: date.getMinutes(),
      });
    }
  };
  const dateValue =
    value === undefined
      ? new Date()
      : dayjs().set('hours', value.hour).set('minutes', value.minutes).toDate();
  return (
    <DateTimePicker
      value={dateValue}
      mode="time"
      onChange={handleChange}
      placeholderText={placeholder}
    />
  );
}

export const TimeInput =
  Platform.OS === 'android' ? AndroidTimeInput : DefaultTimeInput;
