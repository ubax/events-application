import BottomSheet from '@gorhom/bottom-sheet';
import dayjs from 'dayjs';
import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {COLORS} from '../constants';
import {Meeting, User} from '../types';
import {Header, MeetingsTimeList, Spacer} from '.';

interface Props {
  meetings: ReadonlyArray<Meeting>;
  user: User | undefined;
  isOpen: boolean;
  onClose: () => void;
}

const snapPoints = ['60%'];

const NO_MEETINGS_INFO = 'There are no upcoming meetings in this user schedule';

export function UserScheduleBottomSheet({
  user,
  meetings,
  isOpen,
  onClose,
}: Props) {
  const bottomSheetRef = React.useRef<BottomSheet>(null);
  const userMeetings = (user?.meetingIds
    .map(id => meetings.find(m => m.id === id))
    .filter(m => !!m) ?? []) as Meeting[];

  const upcomingMeetings = userMeetings.filter(m =>
    dayjs(m.endTime).isAfter(dayjs()),
  );
  useEffect(() => {
    if (isOpen) {
      bottomSheetRef.current?.snapToIndex(0);
    } else {
      bottomSheetRef.current?.close();
    }
  }, [isOpen]);
  const handleChange = (newIndex: number) => {
    if (newIndex === -1) {
      onClose();
    }
  };
  const meetingsComponent =
    upcomingMeetings.length > 0 ? (
      <MeetingsTimeList
        onSelectMeeting={() => {}}
        selectedMeeting={undefined}
        meetings={upcomingMeetings}
      />
    ) : (
      <Text>{NO_MEETINGS_INFO}</Text>
    );

  return (
    <BottomSheet
      ref={bottomSheetRef}
      index={-1}
      snapPoints={snapPoints}
      onChange={handleChange}
      style={styles.container}
      enablePanDownToClose>
      <View style={styles.content}>
        <Header variant="h3">{user?.name} schedule</Header>
        <Spacer size={1} />
        {meetingsComponent}
      </View>
    </BottomSheet>
  );
}

const styles = StyleSheet.create({
  container: {
    borderColor: COLORS.primary,
    borderWidth: 1,
    borderRadius: 6,
  },
  content: {
    flex: 1,
    backgroundColor: COLORS.white,
    paddingHorizontal: 14,
    paddingVertical: 14,
  },
});
