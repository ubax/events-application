import {StyleSheet} from 'react-native';

export const listStyles = StyleSheet.create({
  container: {flex: 1, flexDirection: 'column'},
  listContainer: {flex: 1},
  list: {paddingTop: 1},
});
