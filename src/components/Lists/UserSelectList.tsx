import React, {useState} from 'react';
import {FlatList, View} from 'react-native';
import {PersonItem} from './Items';
import {Spacer, TextInput} from '..';
import {User} from '../../types';
import {filterUserNamesByText} from '../../utils';
import {listStyles} from './styles';

interface Props {
  users: ReadonlyArray<User>;
  onSelectUser: (user: User) => void;
  selectedUser?: User;
  onShowUserSchedule?: (user: User) => void;
}

export const UserSelectList = ({
  users,
  onSelectUser,
  selectedUser,
  onShowUserSchedule,
}: Props) => {
  const [searchText, setSearchText] = useState('');
  const availableUsers = filterUserNamesByText(users, searchText);

  function renderItem({item}: {item: User}) {
    const isSelected = selectedUser && selectedUser.id === item.id;
    return (
      <PersonItem
        person={item}
        isSelected={isSelected}
        onLongPress={() => onShowUserSchedule?.(item)}
        onPress={() => {
          onSelectUser(item);
        }}
      />
    );
  }

  return (
    <View style={listStyles.container}>
      <TextInput
        value={searchText}
        placeholder="Search"
        onChangeText={setSearchText}
        clearable
      />
      <Spacer size={3} />
      <FlatList
        data={availableUsers}
        renderItem={renderItem}
        keyExtractor={person => person.id}
        style={listStyles.list}
      />
    </View>
  );
};
