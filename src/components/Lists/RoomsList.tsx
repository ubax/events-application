import React from 'react';
import {FlatList, View} from 'react-native';
import {Room} from '../../types';
import {ListItem} from './Items';
import {listStyles} from './styles';

interface Props {
  rooms: ReadonlyArray<Room>;
  onSelectRoom: (room: Room) => void;
  selectedRoom: Room | undefined;
}

export const RoomsList = ({rooms, onSelectRoom, selectedRoom}: Props) => {
  function renderRoomItem({item}: {item: Room}) {
    const isSelected =
      selectedRoom?.id !== undefined && item.id === selectedRoom?.id;
    return (
      <ListItem
        title={`${item.name} (${item.capacity})`}
        isSelected={isSelected}
        onPress={() => onSelectRoom(item)}
      />
    );
  }

  return (
    <View style={listStyles.container}>
      <View style={listStyles.listContainer}>
        <FlatList
          data={rooms}
          renderItem={renderRoomItem}
          keyExtractor={person => person.id}
          style={listStyles.list}
        />
      </View>
    </View>
  );
};
