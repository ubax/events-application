import React from 'react';
import {StyleSheet, View} from 'react-native';
import {User} from '../../types';
import {Chip} from '../Chip';

interface Props {
  participants: ReadonlyArray<User>;
}

export function ParticipantsChipList({participants}: Props) {
  return (
    <View style={styles.container}>
      {participants.map(participant => (
        <Chip
          key={participant.id}
          style={styles.chip}
          title={participant.name}
        />
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    gap: 10,
  },
  chip: {
    marginRight: 12,
    marginBottom: 8,
  },
});
