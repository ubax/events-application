import dayjs from 'dayjs';
import React from 'react';
import {FlatList, View} from 'react-native';
import {MeetingTime} from '../../types';
import {ListItem} from './Items';
import {listStyles} from './styles';

interface Props {
  meetings: ReadonlyArray<MeetingTime>;
  onSelectMeeting: (meeting: MeetingTime) => void;
  selectedMeeting: MeetingTime | undefined;
  showDifference?: boolean;
}

export const MeetingsTimeList = ({
  meetings,
  onSelectMeeting,
  selectedMeeting,
  showDifference,
}: Props) => {
  function renderMeetingItem({item}: {item: MeetingTime}) {
    const isSelected =
      selectedMeeting?.id !== undefined && item.id === selectedMeeting?.id;
    const startTimeDayJS = dayjs(item.startTime);
    const endTimeDayJS = dayjs(item.endTime);
    const startDateTime = startTimeDayJS.format('(dd) YY-MM-DD HH:mm');
    const endDateTime = endTimeDayJS.format('(dd) YY-MM-DD HH:mm');
    const title = `${startDateTime} ${
      showDifference ? `- ${endDateTime}` : ''
    }`;
    return (
      <ListItem
        title={title}
        isSelected={isSelected}
        onPress={() => onSelectMeeting(item)}
      />
    );
  }

  return (
    <View style={listStyles.container}>
      <FlatList
        data={meetings}
        renderItem={renderMeetingItem}
        keyExtractor={person => person.id}
        style={listStyles.list}
      />
    </View>
  );
};
