import React from 'react';
import {StyleSheet, Text, Pressable} from 'react-native';
import {COLORS} from '../../../constants';
import {User} from '../../../types';
import {ListItem} from '.';

interface PersonItemProps {
  person: User;
  onPress?: () => void;
  onLongPress?: () => void;
  onRemove?: () => void;
  isRemovable?: boolean;
  isSelected?: boolean;
}

export function PersonItem({
  person,
  onPress,
  onRemove,
  onLongPress,
  isRemovable,
  isSelected,
}: PersonItemProps) {
  return (
    <ListItem
      onPress={onPress}
      onLongPress={onLongPress}
      title={person.name}
      isSelected={isSelected}
      leftChild={
        isRemovable && (
          <Pressable onPress={onRemove}>
            <Text style={styles.removeButton}>X</Text>
          </Pressable>
        )
      }
    />
  );
}

const styles = StyleSheet.create({
  removeButton: {
    marginRight: 8,
    color: COLORS.primary,
    fontWeight: '900',
  },
});
