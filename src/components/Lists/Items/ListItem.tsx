import React from 'react';
import {StyleSheet, Text, Pressable, View} from 'react-native';
import {COLORS} from '../../../constants';

interface Props {
  title: string;
  leftChild?: React.ReactNode;
  rightChild?: React.ReactNode;
  onPress?: () => void;
  onLongPress?: () => void;
  isSelected?: boolean;
}

export function ListItem({
  title,
  onPress,
  leftChild,
  rightChild,
  isSelected,
  onLongPress,
}: Props) {
  return (
    <View style={[styles.container, isSelected && styles.selected]}>
      {leftChild}
      <Pressable
        onPress={onPress}
        onLongPress={onLongPress}
        style={styles.pressable}>
        <Text style={[styles.text, isSelected && styles.selectedText]}>
          {title}
        </Text>
      </Pressable>
      {rightChild}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: COLORS.gray,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.gray,
    marginTop: -1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  selected: {
    backgroundColor: COLORS.primary,
  },
  selectedText: {
    color: COLORS.white,
    fontWeight: '900',
  },
  pressable: {
    flex: 1,
    paddingVertical: 8,
  },
  text: {
    fontSize: 14,
  },
});
