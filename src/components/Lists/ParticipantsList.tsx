import React from 'react';
import {FlatList, View} from 'react-native';
import {Spacer, Header} from '..';
import {PersonItem} from './Items';
import {User} from '../../types';
import {listStyles} from './styles';

interface Props {
  participants: ReadonlyArray<User>;
  onRemoveParticipant: (participant: User) => void;
  onShowUserSchedule?: (user: User) => void;
}

export const ParticipantsList = ({
  participants,
  onRemoveParticipant,
  onShowUserSchedule,
}: Props) => {
  function renderRemovableItem({item}: {item: User}) {
    return (
      <PersonItem
        person={item}
        isRemovable={true}
        onLongPress={() => onShowUserSchedule?.(item)}
        onRemove={() => {
          onRemoveParticipant(item);
        }}
      />
    );
  }

  return (
    <View style={listStyles.container}>
      <Header variant="h3">Participants</Header>
      <Spacer size={1} />
      <View style={listStyles.listContainer}>
        <FlatList
          data={participants}
          renderItem={renderRemovableItem}
          keyExtractor={person => person.id}
          style={listStyles.list}
        />
      </View>
    </View>
  );
};
