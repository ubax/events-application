import React from 'react';
import {StyleSheet, View} from 'react-native';
import {SelectorItem, TextInput} from './';

type ValueType = string | number;
interface Props {
  predefinedValues: ReadonlyArray<ValueType>;
  onChange: (value: ValueType) => void;
  value: ValueType | undefined;
  hasCustomValueInput?: boolean;
}

export function Selector({
  predefinedValues,
  onChange,
  value,
  hasCustomValueInput,
}: Props) {
  return (
    <View style={styles.container}>
      {predefinedValues.map(pValue => (
        <SelectorItem
          key={pValue}
          value={pValue}
          isSelected={pValue === value}
          onPress={onChange}
        />
      ))}
      {hasCustomValueInput && (
        <TextInput
          placeholder="Custom"
          onChangeText={text => onChange(text)}
          value={value !== undefined ? String(value) : ''}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    maxHeight: 32,
    minHeight: 32,
  },
});
