import React from 'react';
import {StyleSheet, Text, TextProps} from 'react-native';
import {COLORS} from '../constants';

type VariantType = 'h1' | 'h2' | 'h3';

interface Props {
  children: TextProps['children'];
  variant: VariantType;
}

function getStyleForVariant(variant: VariantType) {
  switch (variant) {
    case 'h1':
      return styles.h1;
    case 'h2':
      return styles.h2;
    case 'h3':
      return styles.h3;
  }
  return null;
}

export function Header({children, variant}: Props) {
  return (
    <Text style={[styles.universal, getStyleForVariant(variant)]}>
      {children}
    </Text>
  );
}

const styles = StyleSheet.create({
  universal: {
    color: COLORS.primary,
  },
  h1: {fontSize: 24, fontWeight: '900'},
  h2: {fontSize: 18, fontWeight: '900'},
  h3: {fontSize: 14, fontWeight: '900'},
});
