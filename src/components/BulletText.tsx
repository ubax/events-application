import React from 'react';
import {Text} from 'react-native';

interface IText {
  text: string;
}

export const BulletText = ({text}: IText) => <Text>&#9658; {text}</Text>;
