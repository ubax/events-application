import React from 'react';
import {Pressable, StyleSheet, Text} from 'react-native';
import {COLORS} from '../constants';

type ValueType = string | number;

interface Props {
  value: ValueType;
  onPress: (value: ValueType) => void;
  isSelected: boolean;
}

export function SelectorItem({value, onPress, isSelected}: Props) {
  return (
    <Pressable
      style={[styles.container, isSelected && styles.selectedItem]}
      onPress={() => onPress(value)}>
      <Text style={[styles.itemText]}>{value}</Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
    paddingVertical: 6,
    borderColor: COLORS.primary,
    borderWidth: 1,
    borderRadius: 6,
    color: COLORS.black,
    marginRight: 12,
  },
  selectedItem: {
    color: COLORS.white,
    backgroundColor: COLORS.primary,
  },
  itemText: {},
});
