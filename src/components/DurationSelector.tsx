import React from 'react';
import {Selector} from '.';

interface Props {
  predefinedValues: ReadonlyArray<number>;
  onChange: (value: number | undefined) => void;
  value: number | undefined;
}

export function DurationSelector({predefinedValues, onChange, value}: Props) {
  const handleChange = (v: string | number): void => {
    if (typeof v === 'number') {
      if (isNaN(v)) {
        return onChange(undefined);
      }
      return onChange(v);
    }
    return handleChange(parseInt(v));
  };
  return (
    <Selector
      hasCustomValueInput
      value={value}
      onChange={handleChange}
      predefinedValues={predefinedValues}
    />
  );
}
