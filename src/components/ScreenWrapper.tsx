import React from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';

interface Props {
  children: React.ReactNode;
}

export function ScreenWrapper({children}: Props) {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>{children}</View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {backgroundColor: 'white', flex: 1, padding: 18},
});
