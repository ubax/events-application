import React from 'react';
import {View, StyleSheet, Text, StyleProp, ViewStyle} from 'react-native';
import {COLORS} from '../constants';

interface Props {
  title: string;
  type?: 'filled' | 'outline';
  style?: ViewStyle;
}

function getStyleForType(type: 'filled' | 'outline') {
  switch (type) {
    case 'filled':
      return styles.filled;
    case 'outline':
      return styles.outline;
  }
  return null;
}

export const Chip = (props: Props) => {
  const {title, type, style: _style} = props;
  const style: StyleProp<ViewStyle> = [
    _style,
    styles.chip,
    getStyleForType(type ?? 'outline'),
  ];

  return (
    <View style={style}>
      <Text style={styles.text}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  chip: {
    width: 'auto',
    alignSelf: 'flex-start',
    paddingHorizontal: 6,
    paddingVertical: 4,
    borderRadius: 6,
  },
  filled: {
    backgroundColor: COLORS.primary,
    color: COLORS.white,
  },
  outline: {
    borderWidth: 1,
    borderColor: COLORS.primary,
    color: COLORS.black,
  },
  text: {
    fontSize: 14,
  },
});
