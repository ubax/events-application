import React from 'react';
import {Pressable, PressableProps, StyleSheet, Text} from 'react-native';
import {COLORS} from '../constants';

type Props = Omit<PressableProps, 'style'> & {style: 'primary'; title: string};

export const Button = (props: Props) => {
  const {title, style, disabled, ...rest} = props;
  console.log(title);
  console.log(style);
  return (
    <Pressable
      {...rest}
      disabled={disabled}
      style={[
        style === 'primary' && styles.button,
        disabled && styles.disabled,
      ]}>
      <Text style={styles.buttonText}>{title}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: COLORS.primary,
    flex: 1,
    maxHeight: 36,
  },
  disabled: {
    opacity: 0.6,
  },
  buttonText: {
    color: COLORS.white,
    fontWeight: 'bold',
    padding: 10,
    textAlign: 'center',
  },
});
