import React, {createContext} from 'react';
import {Alert} from 'react-native';
import {DataContextType} from '../types';
import {loadData} from '../utils';

const initialState = {
  data: {users: [], rooms: [], meetings: []},
  setData: (): void => {
    throw new Error('setData function not overwritten');
  },
  loading: true,
};

export const DataContext = createContext<DataContextType>(initialState);

const placeHolderDataObject = {
  users: [],
  rooms: [],
  meetings: [],
};

export const DataContextProvider: React.FunctionComponent = props => {
  const [data, setData] = React.useState<DataContextType['data']>(
    placeHolderDataObject,
  );
  const [isLoading, setIsLoading] = React.useState(true);
  React.useEffect(() => {
    loadData()
      .then(_data => {
        setData(_data);
        setIsLoading(false);
      })
      .catch(_ => {
        Alert.alert('We encountered connection issue');
      });
  }, []);
  return (
    <DataContext.Provider value={{data, setData, loading: isLoading}}>
      {props.children}
    </DataContext.Provider>
  );
};
