import React, {createContext} from 'react';
import {UserContextType} from '../types';

const initialState = {
  user: undefined,
  setUser: () => {
    throw new Error('setUser function not overwritten');
  },
};

const placeUserHolderObject = {
  user: undefined,
};

export const UserContext = createContext<UserContextType>(initialState);

export const UserContextProvider: React.FunctionComponent = props => {
  const [user, setUser] = React.useState<UserContextType['user']>(
    placeUserHolderObject.user,
  );
  return (
    <UserContext.Provider value={{user, setUser}}>
      {props.children}
    </UserContext.Provider>
  );
};
