import {User} from '../types';

export function filterUserNamesByText(
  users: ReadonlyArray<User>,
  text: string,
): ReadonlyArray<User> {
  const lowercaseText = text.toLowerCase();
  return users.filter(({name}) => name.toLowerCase().includes(lowercaseText));
}
