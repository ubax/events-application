import {Meeting} from '../types';
import * as AddCalendarEvent from 'react-native-add-calendar-event';
import {Alert} from 'react-native';

export async function addToCalendar(meeting: Meeting) {
  try {
    const eventConfig = {
      title: meeting.purpose.substring(0, 20),
      startDate: meeting.startTime.toUTCString(),
      endDate: meeting.endTime.toUTCString(),
      notes: meeting.purpose,
    };
    await AddCalendarEvent.presentEventCreatingDialog(eventConfig);
    Alert.alert('Added event to calendar');
  } catch (_) {
    Alert.alert('There was a problem adding your event to calendar');
  }
}
