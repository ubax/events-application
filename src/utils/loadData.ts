import axios from 'axios';
import {BASEURL} from '../constants';
import {DataResponse, Data} from '../types';

function convertResponseToData(response: DataResponse): Data {
  return {
    ...response,
    meetings: response.meetings.map(meeting => ({
      ...meeting,
      startTime: new Date(meeting.startTime),
      endTime: new Date(meeting.endTime),
    })),
  };
}

export const loadData = async () => {
  const response = await axios.get<DataResponse>(BASEURL);
  const fetchedData = response.data;
  return convertResponseToData(fetchedData);
};
