import {Data, Meeting} from '../types';
import uuid from 'react-native-uuid';

export function addMeetingToState(currentState: Data, meeting: Meeting): Data {
  const newId = String(uuid.v4());
  const newUsers = currentState.users.map(user => {
    let meetingIds = [...user.meetingIds];
    if (meeting.userIds.includes(user.id)) {
      meetingIds = [...meetingIds, newId];
    }
    return {
      ...user,
      meetingIds,
    };
  });
  const newMeetings = [...currentState.meetings, {...meeting, id: newId}];
  return {...currentState, users: newUsers, meetings: newMeetings};
}
