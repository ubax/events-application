import dayjs from 'dayjs';
import {Meeting, MeetingTime} from '../types';

export interface FindTimesForMeetingConfig {
  participantsMeetings: ReadonlyArray<MeetingTime>;
  earliestStartTime: string;
  latestEndTime: string;
  minDurationInMinutes: number;
  datesRange: {
    from: Date;
    to: Date;
  };
}

export function sortMeetings(
  meetings: ReadonlyArray<MeetingTime>,
): ReadonlyArray<MeetingTime> {
  const meetingsWithDuration = meetings.map(meeting => ({
    ...meeting,
    duration: meeting.endTime.getTime() - meeting.startTime.getTime(),
  }));
  const sortedMeetings = meetingsWithDuration.sort((m1, m2) => {
    const startTimeDiff = m1.startTime.getTime() - m2.startTime.getTime();
    return startTimeDiff === 0 ? m2.duration - m1.duration : startTimeDiff;
  });
  return sortedMeetings;
}

export function mergeSortedMeetingsToBusyTimes(
  meetings: ReadonlyArray<MeetingTime>,
): ReadonlyArray<MeetingTime> {
  return [...meetings].reduce<MeetingTime[]>((result, meeting) => {
    if (result.length === 0) {
      result.push({...meeting, id: '0'});
    } else {
      const lastMeeting = result[result.length - 1];
      if (meeting.startTime.getTime() > lastMeeting.endTime.getTime()) {
        result.push({...meeting, id: `${result.length}`});
      } else {
        if (meeting.endTime.getTime() > lastMeeting.endTime.getTime()) {
          lastMeeting.endTime = meeting.endTime;
        }
      }
    }
    return result;
  }, []);
}

function getStartAndEndDateFromConfig(
  config: Pick<
    FindTimesForMeetingConfig,
    'datesRange' | 'earliestStartTime' | 'latestEndTime'
  >,
): {startDate: dayjs.Dayjs; endDate: dayjs.Dayjs} {
  const from = dayjs(config.datesRange.from);
  const to = dayjs(config.datesRange.to);
  const isStartingSameDay =
    from.format('HH:mm') < config.earliestStartTime ||
    to.format('HH:mm') > config.latestEndTime;
  const isBetweenDays = config.earliestStartTime > config.latestEndTime;
  const [startHours, startMinutes] = config.earliestStartTime
    .split(':')
    .map(Number);
  const [endHours, endMinutes] = config.latestEndTime.split(':').map(Number);
  const startDayOffset = isStartingSameDay ? 0 : 1;
  const endDayOffset = isBetweenDays ? 1 : 0;
  const startDate = dayjs(from.format('YYYY-MM-DD'))
    .add(startHours, 'hours')
    .add(startMinutes, 'minutes')
    .add(startDayOffset, 'days');
  const endDate = dayjs(from.format('YYYY-MM-DD'))
    .add(endHours, 'hours')
    .add(endMinutes, 'minutes')
    .add(startDayOffset + endDayOffset, 'days');
  return {
    startDate,
    endDate,
  };
}

export function createAvailableIntervals(
  config: Pick<
    FindTimesForMeetingConfig,
    'datesRange' | 'earliestStartTime' | 'latestEndTime'
  >,
): ReadonlyArray<MeetingTime> {
  const from = dayjs(config.datesRange.from);
  const to = dayjs(config.datesRange.to);
  const isStartingSameDay =
    to.format('HH:mm') > config.earliestStartTime &&
    from.format('HH:mm') < config.latestEndTime;
  const numberOfSlots = to.diff(from, 'days') + (isStartingSameDay ? 1 : 0);
  const {startDate, endDate} = getStartAndEndDateFromConfig(config);
  const result = new Array(numberOfSlots).fill(0).map((_, index) => ({
    id: String(index),
    startTime: startDate.add(index, 'days').toDate(),
    endTime: endDate.add(index, 'days').toDate(),
  }));
  if (
    result.length &&
    result[result.length - 1].endTime.getTime() > to.toDate().getTime()
  ) {
    result[result.length - 1].endTime = to.toDate();
  }
  return result;
}

type Interval = Pick<Meeting, 'startTime' | 'endTime'>;

function intervalDurationInSeconds(interval: Interval): number {
  const start = dayjs(interval.startTime);
  const end = dayjs(interval.endTime);
  return end.diff(start, 'seconds');
}

function areIntervalsSorted(intervals: ReadonlyArray<Interval>): boolean {
  return intervals.every((interval, index, arr) => {
    if (index === arr.length - 1) {
      return true;
    }
    const startTime = interval.startTime.getTime();
    const nextStartTime = arr[index + 1].startTime.getTime();
    if (startTime > nextStartTime) {
      return false;
    }
    if (
      startTime === nextStartTime &&
      intervalDurationInSeconds(interval) <
        intervalDurationInSeconds(arr[index + 1])
    ) {
      return false;
    }
    return true;
  });
}

function areIntervalsOverlapping(
  interval1: Interval,
  interval2: Interval,
): boolean {
  return !(
    interval1.startTime.getTime() > interval2.endTime.getTime() ||
    interval2.startTime.getTime() > interval1.endTime.getTime()
  );
}

function diffTwoIntervals(
  interval1: Interval,
  interval2: Interval,
): ReadonlyArray<Interval> {
  const interval1start = interval1.startTime.getTime();
  const interval1end = interval1.endTime.getTime();
  const interval2start = interval2.startTime.getTime();
  const interval2end = interval2.endTime.getTime();
  if (interval1start >= interval2start) {
    if (interval1end <= interval2end) {
      // fully overlapping
      return [];
    }
    // right side
    return [{startTime: interval2.endTime, endTime: interval1.endTime}];
  }
  if (interval1end <= interval2end) {
    // left side
    return [{startTime: interval1.startTime, endTime: interval2.startTime}];
  }
  return [
    {startTime: interval1.startTime, endTime: interval2.startTime},
    {startTime: interval2.endTime, endTime: interval1.endTime},
  ];
}

export function diffIntervals(
  base: Interval,
  intervals: ReadonlyArray<Interval>,
): ReadonlyArray<Interval> {
  if (!areIntervalsSorted(intervals)) {
    throw new Error('Unsorted intervals');
  }
  return intervals.reduce(
    (result, interval) => {
      if (result.length > 0) {
        const lastInterval = result[result.length - 1];
        if (areIntervalsOverlapping(lastInterval, interval)) {
          return [
            ...result.slice(0, -1),
            ...diffTwoIntervals(lastInterval, interval),
          ];
        }
      }
      return result;
    },
    [{...base}],
  );
}

export function findTimesForMeeting(
  config: FindTimesForMeetingConfig,
): ReadonlyArray<MeetingTime> {
  if (config.datesRange.from.getTime() >= config.datesRange.to.getTime()) {
    throw new Error('Date range is incorrect');
  }
  const sortedMeetings = sortMeetings(config.participantsMeetings);
  const busyTimes = mergeSortedMeetingsToBusyTimes(sortedMeetings);
  const availableTimes = createAvailableIntervals(config);

  return availableTimes
    .reduce<ReadonlyArray<Interval>>((result, availableTime) => {
      return [...result, ...diffIntervals(availableTime, busyTimes)];
    }, [])
    .map((interval, index) => ({
      ...interval,
      id: String(index),
    }));
}
