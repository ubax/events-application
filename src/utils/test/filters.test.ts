import {User} from '../../types';
import {filterUserNamesByText} from '../filters';

describe('User by name', () => {
  test('immutability', () => {
    const oldUsers: User[] = [
      {name: 'test', meetingIds: [], id: '1'},
      {name: 'test1', meetingIds: [], id: '2'},
    ];
    const oldUsersCopy = [...oldUsers];

    filterUserNamesByText(oldUsers, 'test');

    expect(
      oldUsersCopy.every((user, index) => oldUsers[index] === user),
    ).toBeTruthy();
    expect(
      oldUsers.every((user, index) => oldUsersCopy[index] === user),
    ).toBeTruthy();
  });
  test('different cases', () => {
    const users: User[] = [
      {name: 'Test', id: '1', meetingIds: []},
      {name: 'Test1', id: '1', meetingIds: []},
      {name: 'TesAt1', id: '1', meetingIds: []},
      {name: 'TEsat1', id: '1', meetingIds: []},
    ];

    const result = [
      {name: 'TesAt1', id: '1', meetingIds: []},
      {name: 'TEsat1', id: '1', meetingIds: []},
    ];

    expect(JSON.stringify(filterUserNamesByText(users, 'a'))).toBe(
      JSON.stringify(result),
    );
  });
});
