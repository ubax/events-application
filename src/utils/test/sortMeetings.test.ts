import {Meeting} from '../../types';
import {sortMeetings} from '../findTimesForMeeting';

describe('sortMeetings', () => {
  test('no meetings', () => {
    const meetings: ReadonlyArray<
      Pick<Meeting, 'startTime' | 'endTime' | 'id'>
    > = [];

    const result = sortMeetings(meetings);

    expect(Array.isArray(result)).toBeTruthy();
    expect(result.length).toBe(0);
  });

  test('unique sorted meetings', () => {
    const meetings: ReadonlyArray<
      Pick<Meeting, 'startTime' | 'endTime' | 'id'>
    > = [
      {
        id: '1',
        startTime: new Date('2022-01-01T20:00:00'),
        endTime: new Date('2022-01-01T22:00:00'),
      },
      {
        id: '2',
        startTime: new Date('2022-01-01T22:00:00'),
        endTime: new Date('2022-01-01T22:30:00'),
      },
      {
        id: '3',
        startTime: new Date('2022-01-02T01:00:00'),
        endTime: new Date('2022-01-02T02:30:00'),
      },
    ];

    const result = sortMeetings(meetings);

    expect(result.length).toBe(3);
    meetings.forEach(meeting => {
      expect(
        result.find(
          m =>
            m.id === meeting.id &&
            m.startTime === meeting.startTime &&
            m.endTime === meeting.endTime,
        ),
      ).toBeTruthy();
    });
    expect(result[0].id).toBe('1');
    expect(result[1].id).toBe('2');
    expect(result[2].id).toBe('3');
  });

  test('unique meetings', () => {
    const meetings: ReadonlyArray<
      Pick<Meeting, 'startTime' | 'endTime' | 'id'>
    > = [
      {
        id: '1',
        startTime: new Date('2022-01-01T22:00:00'),
        endTime: new Date('2022-01-01T22:30:00'),
      },
      {
        id: '2',
        startTime: new Date('2022-01-01T20:00:00'),
        endTime: new Date('2022-01-01T22:00:00'),
      },
      {
        id: '3',
        startTime: new Date('2022-01-02T01:00:00'),
        endTime: new Date('2022-01-02T02:30:00'),
      },
    ];

    const result = sortMeetings(meetings);

    expect(result.length).toBe(3);
    meetings.forEach(meeting => {
      expect(
        result.find(
          m =>
            m.id === meeting.id &&
            m.startTime === meeting.startTime &&
            m.endTime === meeting.endTime,
        ),
      ).toBeTruthy();
    });
    expect(result[0].id).toBe('2');
    expect(result[1].id).toBe('1');
    expect(result[2].id).toBe('3');
  });

  test('not unique meetings', () => {
    const meetings: ReadonlyArray<
      Pick<Meeting, 'startTime' | 'endTime' | 'id'>
    > = [
      {
        id: '3',
        startTime: new Date('2022-01-02T01:00:00'),
        endTime: new Date('2022-01-02T02:30:00'),
      },
      {
        id: '1',
        startTime: new Date('2022-01-01T22:00:00'),
        endTime: new Date('2022-01-01T22:30:00'),
      },
      {
        id: '2',
        startTime: new Date('2022-01-01T20:00:00'),
        endTime: new Date('2022-01-01T22:00:00'),
      },
      {
        id: '3',
        startTime: new Date('2022-01-02T01:00:00'),
        endTime: new Date('2022-01-02T02:30:00'),
      },
      {
        id: '2',
        startTime: new Date('2022-01-01T20:00:00'),
        endTime: new Date('2022-01-01T22:00:00'),
      },
    ];

    const result = sortMeetings(meetings);

    expect(result.length).toBe(5);
    meetings.forEach(meeting => {
      expect(
        result.find(
          m =>
            m.id === meeting.id &&
            m.startTime === meeting.startTime &&
            m.endTime === meeting.endTime,
        ),
      ).toBeTruthy();
    });
    expect(result[0].id).toBe('2');
    expect(result[1].id).toBe('2');
    expect(result[2].id).toBe('1');
    expect(result[3].id).toBe('3');
    expect(result[4].id).toBe('3');
  });

  test('unique meetings not unique times', () => {
    const meetings: ReadonlyArray<
      Pick<Meeting, 'startTime' | 'endTime' | 'id'>
    > = [
      {
        id: '1',
        startTime: new Date('2022-01-02T01:00:00'),
        endTime: new Date('2022-01-02T02:30:00'),
      },
      {
        id: '2',
        startTime: new Date('2022-01-01T22:00:00'),
        endTime: new Date('2022-01-01T22:30:00'),
      },
      {
        id: '3',
        startTime: new Date('2022-01-01T20:00:00'),
        endTime: new Date('2022-01-01T22:00:00'),
      },
      {
        id: '4',
        startTime: new Date('2022-01-02T01:00:00'),
        endTime: new Date('2022-01-02T02:30:00'),
      },
      {
        id: '5',
        startTime: new Date('2022-01-01T20:00:00'),
        endTime: new Date('2022-01-01T22:00:00'),
      },
    ];

    const result = sortMeetings(meetings);

    expect(result.length).toBe(5);
    meetings.forEach(meeting => {
      expect(
        result.find(
          m =>
            m.id === meeting.id &&
            m.startTime === meeting.startTime &&
            m.endTime === meeting.endTime,
        ),
      ).toBeTruthy();
    });
    expect(result[0].id).toBe('3');
    expect(result[1].id).toBe('5');
    expect(result[2].id).toBe('2');
    expect(result[3].id).toBe('1');
    expect(result[4].id).toBe('4');
  });

  test('unique meetings different durations', () => {
    const meetings: ReadonlyArray<
      Pick<Meeting, 'startTime' | 'endTime' | 'id'>
    > = [
      {
        id: '1',
        startTime: new Date('2022-01-02T01:00:00'), //2:3
        endTime: new Date('2022-01-02T03:30:00'),
      },
      {
        id: '2',
        startTime: new Date('2022-01-01T22:00:00'), //1:3
        endTime: new Date('2022-01-01T22:30:00'),
      },
      {
        id: '3',
        startTime: new Date('2022-01-01T20:00:00'), //1:0
        endTime: new Date('2022-01-01T21:00:00'),
      },
      {
        id: '4',
        startTime: new Date('2022-01-02T01:00:00'), //1:3
        endTime: new Date('2022-01-02T02:30:00'),
      },
      {
        id: '5',
        startTime: new Date('2022-01-01T20:00:00'), //2:0
        endTime: new Date('2022-01-01T22:00:00'),
      },
    ];

    const result = sortMeetings(meetings);

    expect(result.length).toBe(5);
    meetings.forEach(meeting => {
      expect(
        result.find(
          m =>
            m.id === meeting.id &&
            m.startTime === meeting.startTime &&
            m.endTime === meeting.endTime,
        ),
      ).toBeTruthy();
    });
    expect(result[0].id).toBe('5');
    expect(result[1].id).toBe('3');
    expect(result[2].id).toBe('2');
    expect(result[3].id).toBe('1');
    expect(result[4].id).toBe('4');
  });
});
