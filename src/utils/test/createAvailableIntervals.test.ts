import {createAvailableIntervals} from '../findTimesForMeeting';
import dayjs from 'dayjs';

// DATE FORMAT:
function expectDateToBe(
  date: Date,
  what: `${number}-${number}-${number | string} ${number}:${number}`,
) {
  expect(dayjs(date).format('YYYY-MM-DD HH:mm')).toBe(what);
}

const padDay = (day: number) => `${day}`.padStart(2, '0');

describe('mergeSortedMeetingsToBusyTimes', () => {
  test('multiple days', () => {
    const result = createAvailableIntervals({
      earliestStartTime: '10:00',
      latestEndTime: '16:00',
      datesRange: {
        from: dayjs('2022-02-02').toDate(),
        to: dayjs('2022-02-08T23:59:00').toDate(),
      },
    });

    expect(result.length).toBe(7);
    result.forEach((meeting, index) => {
      expectDateToBe(meeting.startTime, `2022-02-${padDay(2 + index)} 10:00`);
      expectDateToBe(meeting.endTime, `2022-02-${padDay(2 + index)} 16:00`);
    });
  });
  test('no times after', () => {
    const result = createAvailableIntervals({
      earliestStartTime: '10:00',
      latestEndTime: '16:00',
      datesRange: {
        from: dayjs('2022-02-02T17:00:00').toDate(),
        to: dayjs('2022-02-02T23:59:00').toDate(),
      },
    });

    expect(result.length).toBe(0);
  });
  test('no times before', () => {
    const result = createAvailableIntervals({
      earliestStartTime: '10:00',
      latestEndTime: '16:00',
      datesRange: {
        from: dayjs('2022-02-02T07:00:00').toDate(),
        to: dayjs('2022-02-02T09:59:00').toDate(),
      },
    });

    expect(result.length).toBe(0);
  });
  test('between days', () => {
    const result = createAvailableIntervals({
      earliestStartTime: '20:00',
      latestEndTime: '02:00',
      datesRange: {
        from: dayjs('2022-02-02').toDate(),
        to: dayjs('2022-02-08T23:59:00').toDate(),
      },
    });

    expect(result.length).toBe(7);
    result.slice(0, -1).forEach((meeting, index) => {
      expectDateToBe(meeting.startTime, `2022-02-${padDay(2 + index)} 20:00`);
      expectDateToBe(meeting.endTime, `2022-02-${padDay(2 + 1 + index)} 02:00`);
    });
    expectDateToBe(result[6].startTime, '2022-02-08 20:00');
    expectDateToBe(result[6].endTime, '2022-02-08 23:59');
  });
  test('one day', () => {
    const result = createAvailableIntervals({
      earliestStartTime: '10:00',
      latestEndTime: '13:00',
      datesRange: {
        from: dayjs('2022-02-02').toDate(),
        to: dayjs('2022-02-02T23:59:00').toDate(),
      },
    });

    expect(result.length).toBe(1);
    expectDateToBe(result[0].startTime, '2022-02-02 10:00');
    expectDateToBe(result[0].endTime, '2022-02-02 13:00');
  });
});
