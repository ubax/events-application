import {
  findTimesForMeeting,
  FindTimesForMeetingConfig,
} from '../findTimesForMeeting';
import dayjs from 'dayjs';

function createMeeting(
  dateString: string,
  timeString: string,
  durationInMinutes: number,
): FindTimesForMeetingConfig['participantsMeetings'][0] {
  const startDate = dayjs(`${dateString}T${timeString}+02:00`).toDate();
  return {
    id: `D${dateString}T${timeString}`,
    startTime: startDate,
    endTime: dayjs(startDate).add(durationInMinutes, 'minutes').toDate(),
  };
}

// DATE FORMAT:
function expectDateToBe(
  date: Date,
  what: `${number}-${number}-${number} ${number}:${number}`,
) {
  const timeZone = dayjs().format('Z');
  expect(dayjs(date).format('YYYY-MM-DD HH:mmZ')).toBe(`${what}${timeZone}`);
}

describe('findTimesForMeeting', () => {
  test('start time before end', () => {
    const config: FindTimesForMeetingConfig = {
      participantsMeetings: [
        createMeeting('2022-01-01', '20:00:00', 120),
        createMeeting('2022-01-01', '21:00:00', 90),
        createMeeting('2022-01-02', '01:00:00', 60),
        createMeeting('2022-01-02', '01:15:00', 45),
        createMeeting('2022-01-02', '01:45:00', 120),
      ],
      datesRange: {
        from: new Date('2022-01-01T10:00:00'),
        to: new Date('2022-01-02T12:00:00'),
      },
      earliestStartTime: '20:00',
      latestEndTime: '04:00',
      minDurationInMinutes: 120,
    };

    const result = findTimesForMeeting(config);

    expect(result.length).toBe(2);
    expectDateToBe(result[0].startTime, '2022-01-01 22:30');
    expectDateToBe(result[0].endTime, '2022-01-02 01:00');
  });
  test('no meetings', () => {
    function generateMeetingsForDay(day: number) {
      const dateDay = `2022-01-0${day}`;
      const getHour = (index: number) => `${8 + index * 2}`.padStart(2, '0');
      return new Array(5)
        .fill(0)
        .map((_, index) =>
          createMeeting(dateDay, `${getHour(index + 1)}:00:00`, 120),
        );
    }
    const config: FindTimesForMeetingConfig = {
      participantsMeetings: new Array(5)
        .fill(0)
        .map((_, index) => generateMeetingsForDay(index + 1))
        .reduce((res, x) => [...res, ...x], []),
      datesRange: {
        from: new Date('2022-01-01T00:00:00'),
        to: new Date('2022-01-04T23:59:00'),
      },
      earliestStartTime: '10:00',
      latestEndTime: '16:00',
      minDurationInMinutes: 120,
    };

    const result = findTimesForMeeting(config);

    expect(result.length).toBe(0);
  });
  test('several meetings', () => {
    function generateMeetingsForDay(day: number) {
      const dateDay = `2022-01-0${day}`;
      const getHour = (index: number) =>
        `${6 + (day + index) * 2}`.padStart(2, '0');

      return new Array(3)
        .fill(0)
        .map((_, index) =>
          createMeeting(dateDay, `${getHour(index + 1)}:00:00`, 120),
        );
    }
    const config: FindTimesForMeetingConfig = {
      participantsMeetings: new Array(5)
        .fill(0)
        .map((_, index) => generateMeetingsForDay(index + 1))
        .reduce((res, x) => [...res, ...x], []),
      datesRange: {
        from: new Date('2022-01-01T00:00:00'),
        to: new Date('2022-01-04T23:59:00'),
      },
      earliestStartTime: '10:00',
      latestEndTime: '16:00',
      minDurationInMinutes: 120,
    };

    const result = findTimesForMeeting(config);

    expect(result.length).toBe(3);
    expectDateToBe(result[0].startTime, '2022-01-02 10:00');
    expectDateToBe(result[0].endTime, '2022-01-02 12:00');
    expectDateToBe(result[1].startTime, '2022-01-03 10:00');
    expectDateToBe(result[1].endTime, '2022-01-03 14:00');
    expectDateToBe(result[2].startTime, '2022-01-04 10:00');
    expectDateToBe(result[2].endTime, '2022-01-04 16:00');
  });
});
