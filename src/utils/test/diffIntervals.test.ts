import {diffIntervals} from '../findTimesForMeeting';
import {Meeting} from '../../types';

type Interval = Pick<Meeting, 'startTime' | 'endTime'>;

function expectIntervalsToEqual(interval1: Interval, interval2: Interval) {
  expect(interval1.startTime.toISOString()).toBe(
    interval2.startTime.toISOString(),
  );
  expect(interval1.endTime.toISOString()).toBe(interval2.endTime.toISOString());
}

describe('diff intervals', () => {
  test('one interval', () => {
    const interval = {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    };

    const result = diffIntervals(interval, []);

    expect(result.length).toBe(1);
    expectIntervalsToEqual(result[0], {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    });
  });

  test('two not overlapping intervals', () => {
    const interval = {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    };
    const intervals = [
      {
        startTime: new Date('2022-02-02T10:00:00'),
        endTime: new Date('2022-02-02T20:00:00'),
      },
    ];

    const result = diffIntervals(interval, intervals);

    expect(result.length).toBe(1);
    expectIntervalsToEqual(result[0], {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    });
  });

  test('two overlapping intervals', () => {
    const interval = {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    };
    const intervals = [
      {
        startTime: new Date('2022-02-01T16:00:00'),
        endTime: new Date('2022-02-01T20:00:00'),
      },
    ];

    const result = diffIntervals(interval, intervals);

    expect(result.length).toBe(1);
    expectIntervalsToEqual(result[0], {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T16:00:00'),
    });
  });

  test('two fully overlapping intervals', () => {
    const interval = {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    };
    const intervals = [
      {
        startTime: new Date('2022-02-01T09:00:00'),
        endTime: new Date('2022-02-01T21:00:00'),
      },
    ];

    const result = diffIntervals(interval, intervals);

    expect(result.length).toBe(0);
  });

  test('two overlapping intervals 2', () => {
    const interval = {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    };
    const intervals = [
      {
        startTime: new Date('2022-02-01T17:00:00'),
        endTime: new Date('2022-02-01T22:00:00'),
      },
    ];

    const result = diffIntervals(interval, intervals);

    expect(result.length).toBe(1);
    expectIntervalsToEqual(result[0], {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T17:00:00'),
    });
  });

  test('two continuos intervals', () => {
    const interval = {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    };
    const intervals = [
      {
        startTime: new Date('2022-02-01T20:00:00'),
        endTime: new Date('2022-02-01T22:00:00'),
      },
    ];

    const result = diffIntervals(interval, intervals);

    expect(result.length).toBe(1);
    expectIntervalsToEqual(result[0], {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    });
  });

  test('unsorted intervals', () => {
    const interval = {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    };
    const intervals = [
      {
        startTime: new Date('2022-02-01T20:00:00'),
        endTime: new Date('2022-02-01T22:00:00'),
      },
      {
        startTime: new Date('2022-02-01T10:00:00'),
        endTime: new Date('2022-02-01T20:00:00'),
      },
      {
        startTime: new Date('2022-02-01T20:00:00'),
        endTime: new Date('2022-02-01T22:00:00'),
      },
    ];

    expect(() => diffIntervals(interval, intervals)).toThrowError(
      'Unsorted intervals',
    );
  });

  test('many intervals', () => {
    const interval = {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    };
    const intervals = [
      {
        startTime: new Date('2022-02-01T12:00:00'),
        endTime: new Date('2022-02-01T14:00:00'),
      },
      {
        startTime: new Date('2022-02-01T14:30:00'),
        endTime: new Date('2022-02-01T15:00:00'),
      },
      {
        startTime: new Date('2022-02-01T18:00:00'),
        endTime: new Date('2022-02-01T19:30:00'),
      },
    ];

    const result = diffIntervals(interval, intervals);

    expect(result.length).toBe(4);
    expectIntervalsToEqual(result[0], {
      startTime: new Date('2022-02-01T10:00:00'),
      endTime: new Date('2022-02-01T12:00:00'),
    });
    expectIntervalsToEqual(result[1], {
      startTime: new Date('2022-02-01T14:00:00'),
      endTime: new Date('2022-02-01T14:30:00'),
    });
    expectIntervalsToEqual(result[2], {
      startTime: new Date('2022-02-01T15:00:00'),
      endTime: new Date('2022-02-01T18:00:00'),
    });
    expectIntervalsToEqual(result[3], {
      startTime: new Date('2022-02-01T19:30:00'),
      endTime: new Date('2022-02-01T20:00:00'),
    });
  });
});
