import {
  FindTimesForMeetingConfig,
  mergeSortedMeetingsToBusyTimes,
} from '../findTimesForMeeting';
import dayjs from 'dayjs';

function createMeeting(
  dateString: string,
  timeString: string,
  durationInMinutes: number,
): FindTimesForMeetingConfig['participantsMeetings'][0] {
  const startDate = dayjs(`${dateString}T${timeString}`);
  return {
    id: `D${dateString}T${timeString}`,
    startTime: startDate.toDate(),
    endTime: startDate.add(durationInMinutes, 'minutes').toDate(),
  };
}

// DATE FORMAT:
function expectDateToBe(
  date: Date,
  what: `${number}-${number}-${number} ${number}:${number}`,
) {
  expect(dayjs(date).format('YYYY-MM-DD HH:mm')).toBe(what);
}

describe('mergeSortedMeetingsToBusyTimes', () => {
  test('multiple days', () => {
    const meetings: FindTimesForMeetingConfig['participantsMeetings'] = [
      createMeeting('2022-01-01', '20:00:00', 120),
      createMeeting('2022-01-01', '21:00:00', 90),
      createMeeting('2022-01-02', '01:00:00', 60),
      createMeeting('2022-01-02', '01:15:00', 45),
      createMeeting('2022-01-02', '01:45:00', 120),
    ];

    const result = mergeSortedMeetingsToBusyTimes(meetings);

    expect(result.length).toBe(2);
    expectDateToBe(result[0].startTime, '2022-01-01 20:00');
    expectDateToBe(result[0].endTime, '2022-01-01 22:30');
    expectDateToBe(result[1].startTime, '2022-01-02 01:00');
    expectDateToBe(result[1].endTime, '2022-01-02 03:45');
  });

  test('multiple days continuos', () => {
    const meetings: FindTimesForMeetingConfig['participantsMeetings'] = [
      createMeeting('2022-01-01', '20:00:00', 120),
      createMeeting('2022-01-01', '22:00:00', 180),
      createMeeting('2022-01-02', '01:00:00', 60),
      createMeeting('2022-01-02', '01:15:00', 50),
      createMeeting('2022-01-02', '02:05:00', 65),
    ];

    const result = mergeSortedMeetingsToBusyTimes(meetings);

    expect(result.length).toBe(1);
    expectDateToBe(result[0].startTime, '2022-01-01 20:00');
    expectDateToBe(result[0].endTime, '2022-01-02 03:10');
  });

  test('no meetings', () => {
    const meetings: FindTimesForMeetingConfig['participantsMeetings'] = [];

    const result = mergeSortedMeetingsToBusyTimes(meetings);

    expect(result.length).toBe(0);
  });
  test('same starting time meetings', () => {
    const meetings: FindTimesForMeetingConfig['participantsMeetings'] = [
      createMeeting('2022-01-01', '20:00:00', 120),
      createMeeting('2022-01-01', '20:00:00', 90),
      createMeeting('2022-01-01', '20:00:00', 60),
    ];

    const result = mergeSortedMeetingsToBusyTimes(meetings);

    expect(result.length).toBe(1);
    expectDateToBe(result[0].startTime, '2022-01-01 20:00');
    expectDateToBe(result[0].endTime, '2022-01-01 22:00');
  });
});
