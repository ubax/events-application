export const timeToString = (value: number) => String(value).padStart(2, '0');
