import {Platform} from 'react-native';

// Screens
export const HOME_SCREEN = 'Home';
export const DURATION_SCREEN = 'Duration';
export const PARTICIPANTS_SCREEN = 'Participants';
export const MEETINGS_SCREEN = 'Meetings';
export const DETAILS_SCREEN = 'Details';
export const SUMMARY_SCREEN = 'Summary';

export const INITIAL_FLOW_SCREEN = DURATION_SCREEN;

// Theme
export const COLORS = {
  primary: '#FF7200',
  black: '#111',
  gray: '#A44900',
  white: '#FFF',
};

// API - These constants can be later obtained from env
// https://stackoverflow.com/a/48843551
const BASE_IP = Platform.OS === 'android' ? '10.0.2.2' : 'localhost';
export const BASEURL = `http://${BASE_IP}:3000/response`;
