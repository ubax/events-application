# Source code

## Directories

The code is divided into several directories:
- components - the UI components used on screens
- constants 
- contexts 
- hooks 
- screens - the components used as screens
- types - "global" types in the application. Each component has its own types in its file. The types here are used in the global state of application.
- utils - functions that helps with processing and formatting

## Flow of adding new screen

If you want to add new screen you need to:
1. Create new component for it in `screens` directory
  - Remember to export it in `screens/index.ts` too
2. Add its params to `types/navigatorStack.ts`
3. Add types for it in `types/screens.ts`
4. Add its name to `constants/index.ts`
5. Add it to the stack in `NavigationStack.tsx`

## Flow of adding new component

If you want to add new component:
1. Check if similar component does not exist
  - If it does create wrapper instead of new one
2. Create component in `components`. If you create a set of components that are screen specific or have similar use cases create subfolder for them.
3. Export component in `components/index.ts`