import {User, HourMinutesTime, Meeting, MeetingTime} from '.';

export type StackParamList = {
  Home: {};
  Duration: {};
  Participants: {
    duration: number;
    startTime: HourMinutesTime;
    finishTime: HourMinutesTime;
  };
  Meetings: {
    duration: number;
    startTime: HourMinutesTime;
    finishTime: HourMinutesTime;
    participants: ReadonlyArray<User>;
  };
  Details: {
    duration: number;
    participants: ReadonlyArray<User>;
    meetingTime: MeetingTime;
  };
  Summary: {
    meeting: Meeting;
  };
};
