export type {StackParamList} from './navigatorStack';
export type {
  MeetingsScreenProps,
  ParticipantsScreenProps,
  DurationScreenProps,
  DetailsScreenProps,
  SummaryScreenProps,
  HomeScreenProps,
} from './screens';
export type {DataContextType, UserContextType} from './contexts';
export type {
  User,
  Meeting,
  MeetingTime,
  Room,
  DataResponse,
  Data,
} from './data';

export interface HourMinutesTime {
  hour: number;
  minutes: number;
}
