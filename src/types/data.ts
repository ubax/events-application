export interface Meeting {
  id: string;
  startTime: Date;
  endTime: Date;
  userIds: string[];
  roomId: string;
  purpose: string;
}

export type MeetingTime = Pick<Meeting, 'startTime' | 'endTime' | 'id'>;

export interface Room {
  id: string;
  name: string;
  capacity: number;
}

export interface User {
  id: string;
  name: string;
  meetingIds: string[];
}

export interface Data {
  users: ReadonlyArray<User>;
  rooms: ReadonlyArray<Room>;
  meetings: ReadonlyArray<Meeting>;
}

type ResponseMeeting = Omit<Meeting, 'startTime' | 'endTime'> & {
  startTime: string;
  endTime: string;
};

export interface DataResponse {
  users: User[];
  rooms: Room[];
  meetings: ResponseMeeting[];
}
