import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParamList} from '.';

type ScreenProps<ScreenName extends keyof StackParamList> = {
  route: RouteProp<StackParamList, ScreenName>;
  navigation: StackNavigationProp<StackParamList, ScreenName>;
};

export type HomeScreenProps = ScreenProps<'Home'>;
export type MeetingsScreenProps = ScreenProps<'Meetings'>;
export type ParticipantsScreenProps = ScreenProps<'Participants'>;
export type DurationScreenProps = ScreenProps<'Duration'>;
export type DetailsScreenProps = ScreenProps<'Details'>;
export type SummaryScreenProps = ScreenProps<'Summary'>;
