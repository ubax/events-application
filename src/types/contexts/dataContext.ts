import {Dispatch, SetStateAction} from 'react';
import {Data} from '..';

export type DataContextType = {
  data: Data;
  setData: Dispatch<SetStateAction<Data>>;
  loading: boolean;
};
