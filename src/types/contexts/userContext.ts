import {User} from '..';

export type UserContextType = {
  user: User | undefined;
  setUser: (user: User) => void;
};
