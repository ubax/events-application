import 'react-native-gesture-handler';
import React from 'react';
import {LoadingScreen} from './src/screens/LoadingScreen';
import {useData} from './src/hooks';
import {NavigationStack} from './src/NavigationStack';
import {DataContextProvider, UserContextProvider} from './src/contexts';

function Content() {
  const {loading} = useData();
  const [isOnLoadingEnoughTime, setIsOnLoadingEnoughTime] =
    React.useState(false);
  React.useEffect(() => {
    setTimeout(() => setIsOnLoadingEnoughTime(true), 2000);
  }, []);
  if (loading || !isOnLoadingEnoughTime) {
    return <LoadingScreen />;
  }
  return <NavigationStack />;
}

const Providers: React.FunctionComponent = ({children}) => (
  <DataContextProvider>
    <UserContextProvider>{children}</UserContextProvider>
  </DataContextProvider>
);

const App = () => {
  return (
    <Providers>
      <Content />
    </Providers>
  );
};

export default App;
