# Meetings App

This is simple events application that helps arrange meetings with coworkers.

## Installation

For both platforms you need install js dependencies. To do that run `yarn`.

### iOS

On iOS you also need to install pods. Run `cd ios && pod install && cd ..`.

### Android

## Running

Before running the application you need to start the server. Run `yarn server`.

### iOS

To run iOS application run `yarn ios`.

### Android

To run iOS application run `yarn android`.

### Server

## Future work

In this sections is the list of the things that are not crucial for the application to work but would improve UX significantly.

- [ ] Fix adding events to calendar on Android
- [ ] Add popup signalling why buttons are disabled 
- [ ] Remember the values when going screen back
- [ ] Reset data for initial settings after adding event
- [ ] Add animation for moving element from user to participants list
- [ ] Add better time selector in Details, so that disabled times are more highlighted (can be similar to google calendar style with fixed duration)
- [ ] Add different color to too small rooms with info about that
- [ ] Add check for wether event was actually added to calendar before showing confirmation popup
- [ ] Add spinner for loading data
- [ ] Add splash screen
